\subsection{Machine Learning}
\textit{Machine Learning,} también conocido como aprendizaje automático es una rama de la inteligencia artificial (ver Figura \ref{figura1}), cuyo objetivo es el desarrollo de técnicas que proporcionan a los ordenadores la capacidad de aprender, sin ser explícitamente programados \textbf{Samuel,1956}.
Más tarde \textbf{Tom Mitchell (1998)}, define el aprendizaje automático: \textit{``Dice que un programa de computación aprende de la experiencia E con respecto a una tarea T y alguna medida de rendimiento P, si es que el rendimiento en T, medido por P, mejora con la experiencia E''.}\\ 
\begin{figure}[H]
\centering
\includegraphics[scale=1.1]{./img/ML/ai.jpg}
\caption{Ramas de la inteligencia artificial}
\textit{Fuente: https://bit.ly/2PhWPBn}
\label{figura1}
\end{figure}
El aprendizaje automático es una tecnología que permite a las computadoras aprender directamente de ejemplos y experiencias en forma de datos. Los enfoques tradicionales de programación se basan en reglas codificadas, que establecen cómo resolver un problema, paso a paso. En contraste, los sistemas de aprendizaje automático se configuran como una tarea y se les da una gran cantidad de datos para usar como ejemplos de cómo se puede lograr esta tarea o para detectar patrones. Luego, el sistema aprende la mejor manera de lograr la salida deseada \cite{royal_society_2017}.
\subsubsection*{Objetivos de Machine Learning}
El campo del machine learning se organiza en torno a tres focos principales de investigación:
\begin{itemize}
\item \textbf{Estudio orientado a tareas:} el desarrollo y análisis de sistemas de aprendizaje para mejorar el rendimiento en un conjunto predeterminado de tareas (también conocido como `` enfoque de ingeniería'')
\item \textbf{Simulación cognitiva:} la investigación y simulación por computadora de los procesos de aprendizaje humano.
\item \textbf{Análisis teórico:} la exploración teórica del espacio de posibles métodos de aprendizaje y algoritmos independientes de la aplicación.
\end{itemize} 

Aunque muchos esfuerzos de investigación se esfuerzan principalmente hacia uno de estos objetivos, el progreso hacia un objetivo a menudo conduce al progreso hacia otro.
Por ejemplo, para investigar el espacio de los posibles métodos de aprendizaje, un punto de partida razonable puede ser considerar el único ejemplo conocido de comportamiento de aprendizaje sólido, a saber, los humanos (y quizás otros sistemas biológicos). De manera similar, las investigaciones psicológicas del aprendizaje humano pueden ser ayudadas por un análisis teórico que puede sugerir varios modelos de aprendizaje plausibles. La necesidad de adquirir una forma particular de conocimiento en un estudio orientado a tareas puede generar un nuevo análisis teórico o plantear la pregunta: ``¿Cómo adquieren los humanos esta habilidad específica (o conocimiento)?'' Esta tricotomía de objetivos mutuamente desafiantes y de apoyo es un reflejo de todo el campo de la inteligencia artificial, donde la búsqueda de sistemas expertos, estudios cognitivos y teóricos proporcionan fertilización cruzada de problemas e ideas \cite{michalski_machine_2013}.
\newpage
\subsubsection{Tipos de Machine Learning}
El aprendizaje automático se divide generalmente en dos tipos principales:
\begin{itemize}
\item En el enfoque del \textbf{aprendizaje predictivo o supervisado}, el objetivo es aprender un mapeo de los \textit{inputs x} a los \textit{outputs y}, dado un conjunto etiquetado de pares de salidas de entrada $D = {\{(x_i, y_i)\}}^{N}_{i=1}$, donde $D$ se denomina \textbf{conjunto de entrenamiento}, y $N$ es el número de ejemplos de entrenamiento. 
En la configuración más simple, cada \textit{input} de entrenamiento $x_i$ es un vector de números D-dimensionales, que representa, digamos, la altura y el peso de una persona. Estos se llaman \textbf{características, atributos o co-variables}. En general, sin embargo, $x_i$ podría ser un objeto estructurado complejo, como una imagen, una oración, un mensaje de correo electrónico, una serie de tiempo, una forma molecular, un gráfico, etc.
De manera similar, la forma de la variable de salida \textit{output} o respuesta puede ser en principio cualquier cosa, pero la mayoría de los métodos asumen que $y_i$ es una \textbf{variable categórica o nominal} de un conjunto finito, $y_i \in {1,. . . , C} $ (como masculino o femenino), o que $y_i$ es un valor real escalar (como el nivel de ingresos). Cuando $y_i$ es categórico, el problema se conoce como \textbf{clasificación o reconocimiento de patrones}, y cuando $y_i$ tiene un valor real, el problema se conoce como \textbf{regresión}. Otra variante, conocida como \textbf{regresión ordinal}, ocurre donde el espacio de la etiqueta $y$ tiene algún orden natural, como los grados A a F \cite{murphy_machine_2012}.
\item El segundo tipo principal de aprendizaje automático es el enfoque de \textbf{aprendizaje descriptivo o no supervisado}. Aquí solo se nos dan entradas, $D = {\{x_i\}}^{N}_{i=1}$, y el objetivo es encontrar ``patrones interesantes'' en los datos. Esto a veces se llama \textbf{descubrimiento del conocimiento}. Este es un problema mucho menos definido, ya que no se nos dice qué tipos de patrones buscar, y no hay una métrica de error evidente que usar (a diferencia del aprendizaje supervisado, donde podemos comparar nuestra predicción de $y$ para cada $x$ dada del valor observado) \cite{murphy_machine_2012}.
\item Hay un tercer tipo de aprendizaje automático, conocido como \textbf{aprendizaje por refuerzo},
que es menos utilizado. En un entorno de aprendizaje de refuerzo típico, un agente interactúa con su entorno y recibe una función de recompensa que trata de optimizar, por ejemplo, el sistema puede ser recompensado por ganar un juego. El objetivo del agente es conocer las consecuencias de sus decisiones, por ejemplo, qué movimientos fueron importantes para ganar un juego y usar este aprendizaje para encontrar estrategias que maximicen sus recompensas \cite{royal_society_2017}.
\end{itemize}
\begin{figure}[!ht]
\centering
\includegraphics[scale=0.25]{./img/ML/types.png}
\caption{Tipos de aprendizaje automático}
\textit{Fuente: https://bit.ly/2OexcoQ}
\label{tipos}
\end{figure}
\subsubsection{Aprendizaje automático supervisado}
En los algoritmos de aprendizaje automático supervisado \textit{(Supervised LEarning)}se genera un modelo predictivo, basado en datos de entrada y salida. La palabra clave ``supervisado'' viene de la idea de tener un conjunto de datos previamente etiquetado y clasificado, es decir, tener un conjunto de muestra el cual ya se sabe a qué grupo, valor o categoría pertenecen los ejemplos. Con este grupo de datos, el cual llamamos datos de entrenamiento, se realiza el ajuste al modelo inicial planteado. De esta forma es como el algoritmo va ``aprendiendo'' a clasificar las muestras de entrada comparando el resultado del modelo, y la etiqueta real de la muestra, realizando las compensaciones respectivas al modelo de acuerdo a cada error en la estimación del resultado \cite{gonzalez_tipos_2018}.

Existen dos tipos de aprendizaje automático supervisado:
\begin{enumerate}
\item Clasificación \textit{(Classification)}: en este tipo, el algoritmo encuentra diferentes patrones y tiene por objetivo clasificar los elementos en diferentes grupos.
\item Regresión \textit{(Regression)}: La regresión es como la clasificación, excepto que la variable de respuesta es continua. Tenemos una única entrada de valor real ${x}_i \in \mathbb{R}$, y una única respuesta de valor real ${y}_i \in \mathbb{R}$. Consideramos que se ajustan dos modelos a los datos: una línea recta y una función cuadrática.Surgen varias extensiones de este problema básico, como tener entradas de alta dimensión, valores atípicos, respuestas no uniformes, etc \cite{murphy_machine_2012}. 
\end{enumerate}
\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{./img/ML/supervisado.png}
\caption{Diagrama de flujo del aprendizaje automático supervisado}
\textit{Fuente: https://bit.ly/2C7ATWb}
\label{supervisado}
\end{figure}

\subsubsection{Clasificación}
Es hacer un mapeo de \textit{input} $x$ a sus  \textit{output} $y$, donde $y \in \{1 \ldots C\}$, siendo $C$ el número de clases. Si $C = 2$, se llama \textbf{clasificación binaria} (en cuyo caso a menudo asumimos $y \in \{0, 1\}$); Si $C> 2$, se llama \textbf{clasificación multiclase}. Si las etiquetas de clase no son mutuamente excluyentes (por ejemplo, alguien puede ser clasificado como alto y fuerte), lo llamamos \textbf{clasificación de múltiples etiquetas}, pero esto se ve mejor como predicción de múltiples etiquetas de clase binarias relacionadas (llamado \textbf{modelo de salida múltiple}). Cuando usamos el término ``clasificación'', nos referiremos a la clasificación multiclase con una sola salida, a menos que indiquemos lo contrario.

Una forma de formalizar el problema es una \textbf{función de aproximación}. Asumimos que $y = f (x)$ para alguna función desconocida $f$, y el objetivo del aprendizaje es estimar la función $f$ dado un conjunto de entrenamiento etiquetado, y luego hacer predicciones utilizando $\hat{y} = \hat{f}(x)$. (Usamos el símbolo del sombrero para indicar una estimación). Nuestro objetivo principal es hacer predicciones sobre nuevos \textit{inputs}, es decir, las que no hemos visto antes (esto se llama \textbf{generalización}), ya que predecir la respuesta en el conjunto de entrenamiento es fácil (sólo es buscar la respuesta)\cite{murphy_machine_2012}.

\subsubsection{Enfoques de clasificación}
Entre los diversos enfoques que se pueden aplicar, solo los introduciremos muy brevemente \cite{le_ml}.

\begin{enumerate}
\item \textbf{Árboles de decisión (Decision tree)} El árbol de decisión es un diagrama que representan en forma secuencial condiciones y acciones; muestra qué condiciones se consideran en primer lugar, en segundo lugar y así sucesivamente.
\begin{figure}[!h]
\centering
\includegraphics[scale=0.4]{./img/ML/tree.png}
\caption{Árbol de decisión}
\textit{Fuente: https://bit.ly/2sr2lIn}
\end{figure}
\item \textbf{Vecinos más cercanos ( K-nearest neighbors - kNN)} El algoritmo k-vecinos más cercanos es un clasificador supervisado basado en el reconocimiento de patrones con criterios de vecindad. Parte de que una nueva muestra será clasificada a la clase a la cual pertenezca la mayor cantidad de vecinos más cercanos del conjunto de entrenamiento más cercano a ésta.
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{./img/ML/knn.png}
\caption{K - Vecinos más cercanos}
\textit{Fuente: https://bit.ly/2D2Syju}
\end{figure}
\item \textbf{Clasificador Naïve Bayes (Naïve Bayes classifier)} Los clasificadores Naïve de Bayes son una familia de clasificadores probabilísticos simples basados en la aplicación del teorema de Bayes con supuestos de independencia fuertes entre las características. La ecuación:
\[ P(A|B)= \frac{P(B|A)P(A)}{P(B)} \]
Donde, $P(A|B)$ es la probabilidad posterior,$P (B|A)$ es la probabilidad, $P(A)$ es la probabilidad previa de la clase y $P(B)$ es la probabilidad previa del predictor.
\item \textbf{Máquinas de vectores de soporte (Support vector machines - SVM)} SVM es un algoritmo de clasificación binaria. Dado un conjunto de puntos de 2 tipos en $N$ lugar dimensional, SVM genera un hiperplano dimensional $(N-1)$ para separar esos puntos en 2 grupos. Digamos que tiene algunos puntos de 2 tipos en un papel que son linealmente separables. SVM encontrará una línea recta que separa esos puntos en 2 tipos y se ubica lo más lejos posible de todos esos puntos. 
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{./img/ML/svm.png}
\caption{Vectores de soporte}
\textit{Fuente: https://bit.ly/2Eu7mIV}
\end{figure}
\item \textbf{Redes neuronales artificiales (Artificial neural networks)} Son un paradigma de aprendizaje automático inspirado en
las neuronas de los sistemas nerviosos de los mamíferos.
\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{./img/ML/nn.jpg}
\caption{Redes neuronales artificiales}
\textit{Fuente: https://bit.ly/2sr2lIn}
\end{figure}
\end{enumerate}
